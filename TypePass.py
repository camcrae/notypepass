import pygame, eztext
import menu
import Phrase, Parser
import webbrowser
import os, sys, time, random, hashlib
import tkinter as tk
from tkinter import filedialog
#import NeuralNetwork

class KeyPress:
    def __init__(self, key, dtime):
        self.key = key
        self.dtime = dtime
        self.utime = 0
        self.data = 0

    def calculate(self):
        return self.utime - self.dtime

    def compare(self, other):
        return other.dtime - self.dtime

    def __str__(self):
        return "Key: " + str(self.key) + " Down-Time: " + str(self.dtime) + " Up-Time: " + str(self.utime)

    def __eq__(self, other):
        return self.key == other.key

class TypePass:
    pygame.init()
    pygame.font.init()

    def __init__(self):
        self.screen = pygame.display.set_mode((1280,720))
        pygame.display.set_caption("NoTypePass") #Title of window
        self.menuLevel = 0
        self.screen.fill((211,211,211))
        pygame.scrap.init()
        #Setup font and text
        self.font = pygame.font.Font("coders_crux.ttf", 48)
        self.noticeFont = pygame.font.Font("coders_crux.ttf", 28)
        self.titleText = self.font.render("NoTypePass", True, (0, 200, 255), (211, 211, 211))
        self.titleTextRect = self.titleText.get_rect()
        self.titleTextRect.centerx = self.screen.get_rect().centerx
        self.titleTextRect.centery = self.screen.get_rect().centery - 250

        self.combos = list(range(97,123))
        self.combos.append(32)

        self.pg = Phrase.PhraseGen()
        #this would need to abstracted to the server and auth version instead of the client because we dont want the client to have access to hashes and such
        try:
            f = open('storagekey')
        except:
            self.mp = self.pg.masterPhrase()
            self.mpText = self.noticeFont.render("Master Password: " + self.mp, 1, (10, 10, 10))
            self.mpTextpos = self.mpText.get_rect()
            self.mpTextpos.centerx = self.screen.get_rect().centerx
            self.mpTextpos.centery = self.screen.get_rect().centery + 220
            self.screen.blit(self.mpText, self.mpTextpos)
            self.noticeText = self.noticeFont.render('DO NOT LOSE THIS, THIS IS SHOWN ONLY ONCE. IF LOST, ALL PASSWORDS WILL BE LOST. Value has been copied to clipboard.', 1, (10, 10, 10))
            self.noticeTextpos = self.noticeText.get_rect()
            self.noticeTextpos.centerx = self.screen.get_rect().centerx
            self.noticeTextpos.centery = self.screen.get_rect().centery + 250
            self.screen.blit(self.noticeText, self.noticeTextpos)
            h = hashlib.sha256(self.mp.encode('ascii', 'strict')).hexdigest()
            f = open('storagekey', 'w+')
            f.write(h)
            f.close()
            pygame.scrap.put('text/plain;charset=utf-8', pygame.compat.as_bytes(self.mp))
            pygame.display.flip()

        self.sentence = self.pg.senPhrase()
        self.text = self.font.render("Type the sentence: " + self.sentence, 1, (10, 10, 10))
        self.textpos = self.text.get_rect()
        self.textpos.left = self.screen.get_rect().left
        self.txtbx = eztext.Input(maxlength=100, color=(0,0,255), prompt='Type Here: ', y=40)
        self.clock = pygame.time.Clock()
        self.data = []
        self.key_presses = []
        self.active_key_presses = []

        self.shift_presses = 0

        self.menu1 = menu.Menu() #use menu module to make new menu
        self.menu1.init(['Train', 'Authenticate', 'Quit'], self.screen) #options

        self.eventLoop(self.menu1) #wait for events in menu 1

    def format_data(self, data):
        formatted = []
        for x in range(0, len(data)-2):
            # (key pressed, time key was pressed, time until next keypress)
            formatted.append((data[x][0], data[x+1][2]-data[x][2], data[x+2][2]-data[x+1][2]))
        return formatted

    def is_aunthenticated(self, kp):
        #return random.choice([True, False])
        if self.shift_presses == 2:
            return True
        else:
            return False

    def eventLoopAuthenticate(self):
        while True:
            stime = 0
            etime = 0
            self.clock.tick(60)
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.KEYDOWN:
                    if event.key != 13:
                        stime = time.time()
                        self.data.append((event.key, 'd', stime))
                        self.active_key_presses.append(KeyPress(event.key, stime))
                elif event.type == pygame.KEYUP:
                    if event.key == 27:
                        self.screen.fill((211, 211, 211))
                        pygame.display.flip()
                        self.menu1 = menu.Menu() #use menu module to make new menu
                        self.menu1.init(['Train', 'Authenticate', 'Quit'], self.screen) #options
                        self.eventLoop(self.menu1)
                    elif event.key == 304:
                        self.shift_presses += 1
                    if event.key == 13: #enter is let go
                        if len(self.txtbx.value) == 0:
                            self.txtbx.clear(self.screen)
                        #nn = NeuralNetwork.NerualNetwork([self.parse.averages+self.parse.gaps],[1,0])
                        #nn.cnnInitMLP()
                        elif self.is_aunthenticated(self.key_presses):
                            self.screen.fill((0,255, 0))
                            pygame.display.flip()
                            time.sleep(.25)
                            self.txtbx.clear(self.screen)
                            self.data = []
                            self.active_key_presses = []
                            self.key_presses = []
                            self.screen.fill((211, 211, 211))
                            titleText = self.font.render("Hello, Cameron", True, (0, 50, 255), (211, 211, 211))
                            titleTextRect = self.titleText.get_rect()
                            titleTextRect.centerx = self.screen.get_rect().centerx
                            titleTextRect.centery = self.screen.get_rect().centery - 250
                            self.screen.blit(titleText, titleTextRect)
                            pygame.display.flip()
                            time.sleep(5)
                            webbrowser.open_new_tab('https://bitbucket.org/camcrae/notypepass')
                            self.screen.fill((211, 211, 211))
                            pygame.display.flip()
                            self.shift_presses = 0
                            self.menu1 = menu.Menu() #use menu module to make new menu
                            self.menu1.init(['Train', 'Authenticate', 'Quit'], self.screen) #options
                            self.eventLoop(self.menu1)
                        else:
                            self.screen.fill((255, 0, 0))
                            pygame.display.flip()
                            self.txtbx.clear(self.screen)
                            self.data = []
                            self.active_key_presses = []
                            self.key_presses = []
                            self.sentence = self.pg.senPhrase()
                            self.text = self.font.render("Type the sentence: " + self.sentence, 1, (10, 10, 10))
                            self.screen.fill((255, 0, 0))
                            pygame.display.flip()
                            self.txtbx.clear(self.screen)
                    else:
                        etime = time.time()
                        self.data.append((event.key,'u', etime))
                        for key in self.active_key_presses:
                            if key.key == event.key:
                                key.utime = etime
                                self.key_presses.append(key)
                                self.active_key_presses.remove(key)

            self.screen.fill((211,211,211))
            self.txtbx.update(events)
            self.txtbx.draw(self.screen)
            self.screen.blit(self.text, self.textpos)
            pygame.display.flip()

    def eventLoopTrain(self):
        #root = tk.Tk()
        #root.withdraw()
        #filename = filedialog.askopenfilename() # show an "Open" dialog box and return the path to the selected file
        #print(filename)
        while True:
            stime = 0
            etime = 0
            self.clock.tick(60)
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.KEYDOWN:
                    if event.key != 13:
                        stime = time.time()
                        self.active_key_presses.append(KeyPress(event.key, stime))
                elif event.type == pygame.KEYUP:
                    etime = time.time()
                    if event.key in self.combos:
                        for ind in range(len(self.active_key_presses) - 1):
                            if ind < len(self.active_key_presses):
                                if self.active_key_presses[ind].key == event.key:
                                    self.key_presses.append(self.active_key_presses[ind])
                                    self.key_presses[-1].utime = etime
                                    self.active_key_presses.pop(ind)
                            else:
                                pass
                                #print("Index reference at: ", ind, " while active keys list has a length of: ", len(self.active_key_presses), ".")
                    elif event.key == 27:
                        self.screen.fill((211, 211, 211))
                        pygame.display.flip()
                        self.menu1 = menu.Menu() #use menu module to make new menu
                        self.menu1.init(['Train', 'Authenticate', 'Quit'], self.screen) #options
                        self.eventLoop(self.menu1)
                    elif event.key == 304:
                        self.shift_presses += 1
                    elif event.key == 13: #enter is let go
                        if len(self.txtbx.value) == 0:
                            self.txtbx.clear(self.screen)
                        elif len(self.txtbx.value) < len(self.sentence): #if you fucked up
                            self.sentence = self.pg.senPhrase()
                            self.text = self.font.render("Type the sentence: " + self.sentence, 1, (10, 10, 10))
                            self.screen.fill((255, 0, 0))
                            pygame.display.flip()
                            time.sleep(.25)
                            self.txtbx.clear(self.screen)
                        else:
                            self.screen.fill((0,255, 0))
                            pygame.display.flip()
                            time.sleep(.25)
                            self.txtbx.clear(self.screen)
                            self.parse = Parser.Parser(self.key_presses)
                            self.parse.calculateAvg()
                            self.parse.getElementsIn()
                            self.parse.averages.append(self.shift_presses)
                            f = open('data.txt', 'a')
                            f.write(str(self.parse.averages))
                            f.write('\n')
                            f.write(str(self.parse.gaps))
                            f.write('\n')
                            f.close()
                            print(self.parse.averages)
                            print(self.parse.gaps)
                            self.data = []
                            self.active_key_presses = []
                            self.key_presses = []
                            self.sentence = self.pg.senPhrase()
                            self.text = self.font.render("Type the sentence: " + self.sentence, 1, (10, 10, 10))

            self.screen.fill((211,211,211))
            self.txtbx.update(events)
            self.txtbx.draw(self.screen)
            self.screen.blit(self.text, self.textpos)
            pygame.display.flip()

    def doSomething(self, position):
        """when option is selected, deal with it"""
        if self.menuLevel == 0:
            if position == 0:
                self.screen.fill((211, 211, 211))
                del self.menu1
                self.eventLoopTrain()
            if position == 1:
                self.screen.fill((211, 211, 211))
                del self.menu1
                self.eventLoopAuthenticate()
            if position == 2:
                pygame.quit()
                sys.exit()
            else:
                pygame.quit()
                sys.exit()

    def eventLoop(self, thisMenu):
        """Deal with events to update menu"""
        self.screen.blit(self.titleText, self.titleTextRect)
        thisMenu.draw() #draw current menu
        pygame.display.update() #update screen
        while True: #infinite loop
            for event in pygame.event.get(): #check for events
                if event.type == pygame.KEYDOWN: #
                    if event.key == pygame.K_UP: #if up arrow is pressed
                        thisMenu.draw(-1) #move up on the menu
                    if event.key == pygame.K_DOWN: #if down arrow is pressed
                        thisMenu.draw(1) #move down on the menu
                    if event.key == 13: #if enter is pressed
                        self.doSomething(thisMenu.get_position()) #deal with option selection
                        return #break out of both loops
                    if event.key == pygame.K_ESCAPE: #escape key works to quit program
                        pygame.display.quit() #quit pygame
                        sys.exit() #raise error to exit run
                    pygame.display.update() #update screen

                elif event.type == pygame.QUIT: #X button presson in corner
                    pygame.display.quit() #kill pygame
                    sys.exit() #raise error and kill run

if __name__ == "__main__":
    app = TypePass()